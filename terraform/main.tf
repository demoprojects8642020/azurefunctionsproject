resource "helm_release" "my_app1" {
  name       = "azure-function-example-csharp10"
  chart      = var.helm_chart_path
  version    = "1.0.0"
  namespace  = var.namespace

  set {
    name  = "image.repository"
    value = var.docker_image
  }
}

# provider "azurerm" {
#   features {}
# }

resource "azurerm_resource_group" "rg" {
  name     = "rg"
  location = "West US 2"
}

resource "azurerm_function_app" "myFunctionApp" {
  name                      = "myFunctionApp"
  location                  = azurerm_resource_group.rg.location
  resource_group_name       = azurerm_resource_group.rg.name
  app_service_plan_id       = azurerm_app_service_plan.myAppServicePlan.id
  storage_account_name      = azurerm_storage_account.mystorageaccount.name
  storage_account_access_key = azurerm_storage_account.mystorageaccount.primary_access_key
  version                   = "~3"
  os_type                   = "linux"

  app_settings = {
    "FUNCTIONS_WORKER_RUNTIME" = "node"
  }
}


resource "azurerm_app_service_plan" "myAppServicePlan" {
  name                = "myAppServicePlan"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  kind                = "FunctionApp"
  sku {
    tier = "Dynamic"
    size = "Y1"
  }
}

resource "azurerm_storage_account" "mystorageaccount" {
  name                     = "mystorageaccount"
  resource_group_name      = azurerm_resource_group.rg.name
  location                 = azurerm_resource_group.rg.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
}



