variable "docker_image" {
  description = "The Docker image to deploy"
  type        = string
  default     = "your-docker-image:latest"
}


variable "helm_chart_path" {
  description = "The Helm chart path"
  type        = string
  default     = "/path/to/default/helm/chart"
}



variable "namespace" {
  description = "Environment for deployment"
  type        = string
  default     = "default-namespace"
}

