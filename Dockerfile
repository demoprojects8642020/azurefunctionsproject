FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build-env

WORKDIR /app

COPY src/*.csproj ./src/
RUN dotnet restore src/

COPY . ./
RUN dotnet publish -c Release -o out src/

FROM mcr.microsoft.com/dotnet/aspnet:7.0
WORKDIR /app
COPY --from=build-env /app/out .
ENTRYPOINT ["dotnet", "azure-function-example-csharp.dll"]
